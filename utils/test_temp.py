import os
import json
import datetime
from PySide2 import QtWidgets, QtCore, QtGui

from ocr import extract_text
from ui import ui_config

TEMPLATES_DIR = os.path.join(QtCore.QDir.currentPath(), "templates")

cfg = ui_config.cfg(lang='kor')


def test_template(t, templates, test_dir, id_card=False, ocr_engine="Tesseract"):
    test_files = os.listdir(test_dir)
    allowed_extensions = ['.jpg', '.png', '.JPG', '.jpeg', '.PNG']
    test_files = [os.path.join(test_dir, x) for x in test_files if os.path.splitext(x)[1] in allowed_extensions]

    now = datetime.datetime.now()
    strnow = now.strftime("%Y%m%d_%H%M%S")

    out_path = os.path.join(test_dir, strnow + "_" + ocr_engine[0])
    os.mkdir(out_path)

    progress = QtWidgets.QProgressDialog(t)
    progress.setWindowModality(QtCore.Qt.WindowModal)
    progress.setWindowTitle(cfg.running_test_str)
    progress.setMinimum(0)
    # progress.setLabelText("Extracting text from {} images...".format(len(test_files)))
    progress.setLabelText(cfg.total_img_str.format(len(test_files)))
    progress.setCancelButtonText(cfg.abort_str)
    progress.setMaximum(len(test_files))
    progress.setMinimumSize(400, 100)
    progress.setAutoReset(False)
    progress.setAutoClose(False)
    progress.setValue(0)
    progress.show()

    tp_json = []
    for t in templates:
        with open(os.path.join(TEMPLATES_DIR, t), 'r') as f:
            tp = json.load(f)
        tp_json.append(tp)

    for i in range(len(test_files)):
        progress.setValue(i)
        QtGui.QGuiApplication.processEvents()
        extract_text(tp_json, test_files[i], id_card, ocr_engine, out_path)
        if progress.wasCanceled():
            break

    progress.setValue(progress.maximum())
    progress.setWindowTitle(cfg.done_str)
    progress.setCancelButtonText(cfg.done_str)
    progress.setLabelText(cfg.saved_res_str.format(out_path))
