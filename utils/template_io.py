from collections import OrderedDict

from PySide2 import QtCore, QtWidgets

from .tab_template import TabPage
from .rubberband import ResizableRubberBand


def create_template(t):
    template = OrderedDict()
    template.update({"template_name": t.template_name.text()})
    template.update({"template_size": [t.label.pixmap().width(), t.label.pixmap().height()]})
    template.update({"head_field_texts": t.headtextEdit.toPlainText().strip(" ").split(',')})

    ocr_fields = []
    for i in range(t.tabWidget.count()):
        curr_tab = t.tabWidget.widget(i)

        if curr_tab.head_field_check.checkState() == QtCore.Qt.CheckState.Checked:
            template.update({"head_field_idx": i})

        field = {}
        field.update({"id": list(t.ocr_fields.keys())[i]})
        field.update({"head_field": curr_tab.head_field})
        field.update({"field_name": curr_tab.field_name.text()})

        rband = t.ocr_fields[list(t.ocr_fields.keys())[i]]
        rect = rband.geometry()
        rect = rect.getRect()
        rect = [int(x * t.label.pixmap().width()/t.label.width()) if i % 2 else
                int(x * t.label.pixmap().height()/t.label.height()) for x in rect]
        field.update({"bbox": rect})

        types = []
        check_list = curr_tab.groupBox_2.findChildren(QtWidgets.QCheckBox)
        for box in check_list:
            if box.isChecked():
                bt = box.text()
                if bt == "영어":
                    bt = "English"
                elif bt == "한국어":
                    bt = "Korean"
                elif bt == "숫자":
                    bt = "Numbers"

                types.append(bt)
        field.update({"type": types})

        field.update({"char_blacklist": curr_tab.char_black.toPlainText().strip(" ")})
        ocr_fields.append(field)

    template.update({"ocr_fields": ocr_fields})

    return template


def load_template(t, temp_dict):
    t.label.setPixmap(t.label.pixmap().scaled(*temp_dict['template_size']))
    t.template_name.setText(temp_dict['template_name'])

    t.headtextEdit.setText(", ".join(temp_dict['head_field_texts']))
    t.head_set = True

    for field in temp_dict['ocr_fields']:
        tab = TabPage(t.tabWidget)

        if field['head_field']:
            tab.head_field_check.setCheckState(QtCore.Qt.CheckState.Checked)
            tab.head_field_check.setEnabled(True)
            tab.head_field = True
            t.head_set = True

        tab.field_name.setText(field['field_name'])

        rband = ResizableRubberBand(t.label, field['id'])
        rband.setGeometry(*field['bbox'])

        t.ocr_fields.update({field['id']: rband})
        clist = tab.groupBox_2.findChildren(QtWidgets.QCheckBox)
        for box in clist:
            bt = box.text()
            if bt == "영어":
                bt = "English"
            elif bt == "한국어":
                bt = "Korean"
            elif bt == "숫자":
                bt = "Numbers"
                
            if bt in field['type']:
                box.setCheckState(QtCore.Qt.CheckState.Checked)
            else:
                box.setCheckState(QtCore.Qt.CheckState.Unchecked)

        tab.char_black.setText(field['char_blacklist'])
        t.tabWidget.addTab(tab, str(field['id']))
