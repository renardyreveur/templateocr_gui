from PySide2 import QtWidgets, QtCore, QtGui


class ResizableRubberBand(QtWidgets.QWidget):
    def __init__(self, parent=None, f_id=0):
        super(ResizableRubberBand, self).__init__(parent)

        self.draggable = True
        self.dragging_threshold = 2
        self.mousePressPos = None
        self.mouseMovePos = None
        self.borderRadius = 5
        self.f_id = f_id
        self.factor = 1

        self.setWindowFlags(QtCore.Qt.SubWindow)
        layout = QtWidgets.QHBoxLayout(self)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(
            QtWidgets.QSizeGrip(self), 0,
            QtCore.Qt.AlignLeft | QtCore.Qt.AlignTop)
        layout.addWidget(
            QtWidgets.QSizeGrip(self), 0,
            QtCore.Qt.AlignRight | QtCore.Qt.AlignBottom)
        self._band = QtWidgets.QRubberBand(
            QtWidgets.QRubberBand.Rectangle, self)

        self._band.show()
        self.show()

    def resizeEvent(self, event):
        self._band.resize(self.size())

    def paintEvent(self, event):
        # Get current window size
        window_size = self.size()
        qp = QtGui.QPainter()
        qp.begin(self)
        qp.setRenderHint(QtGui.QPainter.Antialiasing, True)
        if int(self.window().tabWidget.tabText(self.window().tabWidget.currentIndex())) == self.f_id:
            qp.fillRect(self.rect(), QtGui.QBrush(QtGui.QColor(128, 200, 255, 50)))
        else:
            qp.fillRect(self.rect(), QtGui.QBrush(QtGui.QColor(5, 5, 25, 120)))

        qp.drawRoundedRect(0, 0, window_size.width(), window_size.height(),
                           self.borderRadius, self.borderRadius)
        font = qp.font()
        font.setPointSize(font.pointSize()*(window_size.height()//50))
        qp.setFont(font)
        qp.drawText(self.rect(), QtCore.Qt.AlignCenter, str(self.f_id))
        qp.end()

    def mouseDoubleClickEvent(self, event):
        this_tab = -1
        for i in range(self.window().tabWidget.count()):
            if int(self.window().tabWidget.tabText(i)) == self.f_id:
                this_tab = i
        self.window().tabWidget.setCurrentIndex(this_tab)

    def mousePressEvent(self, event):
        if self.draggable and event.button() == QtCore.Qt.LeftButton:
            self.mousePressPos = event.globalPos()                # global
            self.mouseMovePos = event.globalPos() - self.pos()    # local
        super(ResizableRubberBand, self).mousePressEvent(event)

    def mouseMoveEvent(self, event):
        if self.draggable and event.buttons() & QtCore.Qt.LeftButton:
            globalPos = event.globalPos()
            moved = globalPos - self.mousePressPos

            if moved.manhattanLength() > self.dragging_threshold:
                # Move when user drag window more than dragging_threshold
                diff = globalPos - self.mouseMovePos
                if self.parent().width() - self.width() > diff.x() > 0 and self.parent().height() - self.height() > diff.y() > 0:
                    self.move(diff)
                    self.mouseMovePos = globalPos - self.pos()
        super(ResizableRubberBand, self).mouseMoveEvent(event)

    def mouseReleaseEvent(self, event):
        if self.mousePressPos is not None:
            if event.button() == QtCore.Qt.LeftButton:
                moved = event.globalPos() - self.mousePressPos
                if moved.manhattanLength() > self.dragging_threshold:
                    # Do not call click event or so on
                    event.ignore()
                self.mousePressPos = None
        super(ResizableRubberBand, self).mouseReleaseEvent(event)
