from PySide2 import QtWidgets, QtCore
from PySide2.QtWidgets import QFileDialog

from ui import Ui_test_dialog, ui_config
from .test_temp import test_template


class TestPage(QtWidgets.QDialog, Ui_test_dialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)

        self.cfg = ui_config.cfg(lang='kor')

        self.pushButton.clicked.connect(self.select_all)
        self.pushButton_2.clicked.connect(self.deselect_all)
        self.pushButton_3.clicked.connect(self.select_folder)
        self.template_list.itemDoubleClicked.connect(self.select_item)
        self.ok.clicked.connect(self.ok_slot)
        self.cancel.clicked.connect(self.cancel_slot)

        self.test = False
        self.test_templates = []
        self.test_folder = None

    def ok_slot(self):
        no_select = 0
        for i in range(self.template_list.count()):
            cs = self.template_list.item(i).checkState()
            if cs == QtCore.Qt.Unchecked:
                no_select += 1

        if self.lineEdit.text() == "":
            msgBox = QtWidgets.QMessageBox()
            msgBox.setWindowTitle(self.cfg.error_str)
            msgBox.setText(self.cfg.test_folder_str)
            msgBox.exec_()

        elif no_select == self.template_list.count():
            msgBox = QtWidgets.QMessageBox()
            msgBox.setWindowTitle(self.cfg.error_str)
            msgBox.setText(self.cfg.one_temp_str)
            msgBox.exec_()

        else:
            for i in range(self.template_list.count()):
                if self.template_list.item(i).checkState() == QtCore.Qt.CheckState.Checked:
                    self.test_templates.append(self.template_list.item(i).text()+".json")
            self.test_folder = self.lineEdit.text()
            self.test = True

        if self.test and len(self.test_templates) > 0 and self.test_folder is not None:
            id_mode = self.parent().groupBox.findChildren(QtWidgets.QRadioButton)[0].isChecked()
            engines = self.parent().groupBox_2.findChildren(QtWidgets.QRadioButton)
            engine = [x.text() for x in engines if x.isChecked()][0]
            if engine == "모델 A":
                engine = "Tesseract"
            elif engine == "모델 B":
                engine = "EasyOCR"
            else:
                engine = "ComtrueOCR"
            test_template(self, self.test_templates, self.test_folder, id_card=id_mode, ocr_engine=engine)

    def cancel_slot(self):
        self.test = False
        self.close()

    @staticmethod
    def select_item(item):
        if item.checkState() == QtCore.Qt.CheckState.Checked:
            item.setCheckState(QtCore.Qt.Unchecked)
        else:
            item.setCheckState(QtCore.Qt.Checked)

    def select_all(self):
        for i in range(self.template_list.count()):
            self.template_list.item(i).setCheckState(QtCore.Qt.Checked)

    def deselect_all(self):
        for i in range(self.template_list.count()):
            self.template_list.item(i).setCheckState(QtCore.Qt.Unchecked)

    def select_folder(self):
        dialog = QFileDialog(self)
        dialog.setOption(QtWidgets.QFileDialog.DontUseNativeDialog)
        # dialog.setOption(QFileDialog.ShowDirsOnly)
        dialog.setWindowTitle(self.cfg.test_fol_dia_str)
        dialog.setFileMode(QFileDialog.Directory)

        dir_name = None
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            dir_name = dialog.selectedFiles()[0]

        if dir_name is not None:
            self.lineEdit.setText(dir_name)
