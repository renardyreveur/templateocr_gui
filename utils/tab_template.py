from PySide2 import QtWidgets, QtCore
from ui import Ui_TabPage


class TabPage(QtWidgets.QWidget, Ui_TabPage):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.parent = parent
        self.pushButton.clicked.connect(self.removeField)
        self.head_field = False
        self.head_field_check.stateChanged.connect(self.set_head)
        if self.window().head_set is True:
            self.head_field_check.setDisabled(True)

        self.eng_type.stateChanged.connect(self.set_type)
        self.kor_type.stateChanged.connect(self.set_type)
        self.num_type.stateChanged.connect(self.set_type)
        self.code_type.stateChanged.connect(self.set_type)

    def set_type(self):
        if self.code_type.checkState() != QtCore.Qt.CheckState.Unchecked:
            self.eng_type.setCheckState(QtCore.Qt.Unchecked)
            self.kor_type.setCheckState(QtCore.Qt.Unchecked)
            self.num_type.setCheckState(QtCore.Qt.Unchecked)
            self.eng_type.setDisabled(True)
            self.kor_type.setDisabled(True)
            self.num_type.setDisabled(True)
        else:
            self.eng_type.setEnabled(True)
            self.kor_type.setEnabled(True)
            self.num_type.setEnabled(True)

        if self.eng_type.checkState() == QtCore.Qt.Unchecked and self.kor_type.checkState() == QtCore.Qt.Unchecked and self.num_type.checkState() == QtCore.Qt.Unchecked:
            self.code_type.setEnabled(True)
        else:
            self.code_type.setDisabled(True)

    def set_head(self):
        if self.head_field_check.checkState() != QtCore.Qt.CheckState.Unchecked:
            if self.window().head_set is False:
                for i in range(self.parent.count()):
                    if i != self.parent.currentIndex():
                        self.parent.widget(i).head_field_check.setDisabled(True)
        else:
            for i in range(self.parent.count()):
                self.parent.widget(i).head_field_check.setEnabled(True)

        self.head_field = not self.head_field
        self.window().head_set = not self.window().head_set

    def removeField(self):
        # TODO : Reorganise indices such that once one is deleted it is shifted to fill the gap
        # Pop the widget
        self.parent.parent().parent().ocr_fields[int(self.parent.tabText(self.parent.currentIndex()))].close()
        self.parent.parent().parent().ocr_fields[int(self.parent.tabText(self.parent.currentIndex()))].deleteLater()

        # Remove from the Dictionary holding idx:rubberband
        self.parent.parent().parent().ocr_fields.pop(int(self.parent.tabText(self.parent.currentIndex())))

        # Re-enable others
        if self.head_field:
            for i in range(self.parent.count()):
                self.parent.widget(i).head_field_check.setEnabled(True)

            self.window().head_set = False

        # Remove this tab
        self.parent.removeTab(self.parent.currentIndex())


