import numpy as np
import itertools


def contrast_grey(img):
    high = np.percentile(img, 90)
    low = np.percentile(img, 10)
    return (high-low)/np.maximum(10, high+low), high, low


def adjust_contrast_grey(img, target=0.4):
    contrast, high, low = contrast_grey(img)
    if contrast < target:
        img = img.astype(int)
        ratio = 200./np.maximum(10, high-low)
        img = (img - low + 25)*ratio
        img = np.maximum(np.full(img.shape, 0), np.minimum(np.full(img.shape, 255), img)).astype(np.uint8)
    return img


def softmax(X, axis=None):
    # make X at least 2d
    y = np.atleast_2d(X)

    # find axis
    if axis is None:
        axis = next(j[0] for j in enumerate(y.shape) if j[1] > 1)

    # subtract the max for numerical stability
    y = y - np.expand_dims(np.max(y, axis=axis), axis)

    # Exponentiate y
    y = np.exp(y)

    # take the sum along the specified axis
    ax_sum = np.expand_dims(np.sum(y, axis=axis), axis)
    # finally: divide elementwise
    p = y / ax_sum

    # flatten if X was 1D
    if len(X.shape) == 1: p = p.flatten()

    return p


def greedy_decode(output, rd):
    min_conf = 0.2

    # Get the highest probability outputs of each output
    result = np.argmax(output, axis=2)[0]

    # Add blank to dictionary
    rd.update({0: "_"})

    # Decode into string
    result_text = ''.join([rd[x] if output[0][i, x] > min_conf else " " for i, x in enumerate(result)])

    # Remove double letters
    decoded = ''.join([i[0] for i in itertools.groupby(result_text)])

    # Remove blanks
    decoded = decoded.replace('_', '')

    return decoded
