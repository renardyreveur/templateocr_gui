import os

import onnxruntime


class comtrueOCR:
    def __init__(self):
        self.imgH = 58
        # Vocabulary
        with open(os.path.join('models', 'comtrueOCR', 'vocab.txt'), 'r', encoding='utf-8') as tf:
            vocab = tf.readlines()

        # Dictionaries for Vocab-ID
        vocab = [x.strip() for x in vocab]
        self.char2id = {x.strip(): i + 1 for i, x in enumerate(vocab)}
        self.id2char = {i + 1: x.strip() for i, x in enumerate(vocab)}

        self.ignore_char = ""

        # model_file = os.path.join('models', 'comtrueOCR', "comtrueOCR_v0_1.onnx")
        # model_file = os.path.join('models', 'comtrueOCR', "comtrueOCR_v0.1_mob.onnx")
        model_file = os.path.join('models', 'comtrueOCR', "comtrueOCR_v0.1_rex.onnx")

        self.ort_session = onnxruntime.InferenceSession(model_file)

