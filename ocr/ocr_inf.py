import os
import cv2
import csv
import ocr
from difflib import SequenceMatcher


def similar(a, b):
    return SequenceMatcher(None, a, b).ratio()


def extract_text(templates, test_file, id_card, ocr_engine, out_path):
    """
    :param templates: List of dictionaries holding template information
    :param test_file: path to test image
    :param id_card: Boolean whether to use id_card classification to select template
    :param ocr_engine: String, one of ["Tesseract", "EasyOCR", "ComtrueOCR"]
    :param out_path: Path to directory that will hold the results
    """
    out_file = os.path.splitext(os.path.basename(test_file))[0] + ".csv"

    head_regions = {}
    for t in templates:
        head_idx = t['head_field_idx']
        fields = t['ocr_fields']
        hr = fields[head_idx]["bbox"]
        head_regions.update({t["template_name"]: [hr, fields[head_idx], t["head_field_texts"], t["template_size"]]})

    image = cv2.imread(test_file)
    ocr_engine = getattr(ocr, ocr_engine)

    doc_type = "other"
    max_sim = 0.5
    for head, info in head_regions.items():
        bbox = info[0]
        lang = info[1]["type"]
        ignore = info[1]["char_blacklist"]
        text = info[2][0]
        size = info[3]

        test_img = cv2.resize(image, tuple(size))
        region = test_img[bbox[1]:bbox[1]+bbox[3], bbox[0]:bbox[0]+bbox[2]]
        result = ocr_engine(region, lang, ignore)
        similarity = similar(result, text)

        if similarity > max_sim:
            doc_type = head
            max_sim = similarity

    if doc_type != "other":
        template = [t for t in templates if t['template_name'] == doc_type][0]
    else:
        template = None

    if template is None:
        with open(os.path.join(out_path, out_file), 'w', encoding='utf-8') as f:
            result_writer = csv.writer(f, delimiter=',')
            result_writer.writerow(["This image does not match any of the selected templates!"])

        return 0

    else:
        # Extract Chosen Template Info.
        doc_type = template["template_name"]
        temp_size = template["template_size"]
        ocr_fields = template['ocr_fields']
        sorted(ocr_fields, key=(lambda x: x['bbox'][1]), reverse=True)

        # Resize image to fit template
        image = cv2.resize(image, tuple(temp_size))

        result_dict = []
        # Iterate over each fields
        for field in ocr_fields:
            field_name = field['field_name']
            bbox = field['bbox']
            lang = field['type']
            ignore = field['char_blacklist']

            # Crop region
            region = image[bbox[1]:bbox[1]+bbox[3], bbox[0]:bbox[0]+bbox[2]]

            # Run OCR
            result = ocr_engine(region, lang, ignore)

            # Append to Dict
            result_dict.append({"Field Name": field_name, "Value": result})

        with open(os.path.join(out_path, out_file), 'w', encoding='utf-8') as f:
            headers = ["Field Name", "Value"]
            dict_writer = csv.DictWriter(f, fieldnames=headers)
            result_writer = csv.writer(f, delimiter=',')

            result_writer.writerow(['This document is classified as:', doc_type])
            result_writer.writerow([])

            dict_writer.writeheader()
            for result in result_dict:
                dict_writer.writerow(result)

        return 0





