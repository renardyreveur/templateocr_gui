import os
import onnxruntime


class easyOCR:
    def __init__(self):
        # --------- PARAMETERS ----------
        self.imgH = 64
        BASE_PATH = './'
        lang = 'korean'

        if lang == 'latin':
            ignore_char = "ÀÁÂÃÄÅÆÇÈÉÊËÍÎÑÒÓÔÕÖØÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýþÿąęĮįıŁłŒœŠšųŽž"
        else:
            ignore_char = ""

        ignore_sym = '!"#$%&\'()*+,:;<=>?@[\\]^_`{|}~'
        self.ignore_char = ignore_char + ignore_sym
        # -------------------------------

        # ------- DATA PREPARATION -------
        # Formulate Vocabulary for OCR Inference
        number = '0123456789'
        symbol = '!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~ '
        en_char = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'

        if lang == 'latin':
            all_char = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz' + \
                       'ÀÁÂÃÄÅÆÇÈÉÊËÍÎÑÒÓÔÕÖØÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýþÿąęĮįıŁłŒœŠšųŽž'
            character = number + symbol + all_char
            model_file = os.path.join("models", "easyOCR", "EOCR_eng.onnx")

        elif lang == 'korean':
            char_file = os.path.join(BASE_PATH, 'character', "ko_char.txt")
            with open(char_file, "r", encoding="utf-8-sig") as input_file:
                ko_list = input_file.read().splitlines()
                ko_char = ''.join(ko_list)
            self.ko_char = ko_char
            character = number + symbol + en_char + ko_char
            model_file = os.path.join("models", "easyOCR", "EOCR_kor.onnx")
        # -------------------------------------------

        # Dictionaries id - char
        self.id2char = {x + 1: y for x, y in enumerate(character)}
        self.char2id = {y: x + 1 for x, y in enumerate(character)}

        # Onnxruntime Session
        self.ort_session = onnxruntime.InferenceSession(model_file)
