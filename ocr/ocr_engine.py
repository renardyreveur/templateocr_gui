import pytesseract
import cv2
import numpy as np
from pylibdmtx.pylibdmtx import decode as decode_dm
from pyzbar.pyzbar import decode as decode_qr

from .ocr_utils import softmax, greedy_decode
from .easyOCR import easyOCR
from .comtrueOCR import comtrueOCR

# EasyOCR params
easy_params = easyOCR()

# ComtrueOCR Params
comtrue_params = comtrueOCR()


def roundup(x):
    return ((x + 7) & (-8)) - 2


def CodeDecoder(image):
    # QR / Barcode
    decoded = decode_qr(image)
    if len(decoded) != 0:
        return decoded[0][0].decode("utf-8")
    # DM
    dm_img = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    _, dm_img = cv2.threshold(dm_img, 200, 255, cv2.THRESH_BINARY)
    decoded = decode_dm(dm_img)
    if len(decoded) != 0:
        return decoded[0][0].decode("utf-8")
    return ""


# Tesseract Inference
def Tesseract(image, lang, ignore):
    if "QR/DM/Bar" in lang:
        output = CodeDecoder(image)
    else:
        td_lang = None
        if "Korean" in lang and "English" in lang:
            td_lang = "extsen3_1_3"
        elif "Korean" in lang:
            td_lang = "kor"
        elif "English" in lang or "Numbers" in lang:
            td_lang = "eng"

        output = pytesseract.image_to_string(image, lang=td_lang, config='--psm 6 --tessdata-dir ./models/tesseract')

    return output


def EasyOCR(image, lang, ignore):
    if "QR/DM/Bar" in lang:
        decoded = CodeDecoder(image)
    else:
        all_lang = ["Korean", "English", "Numbers"]
        ignore_lang = list(set(all_lang).difference(lang))
        for lang in ignore_lang:
            if lang == "Korean":
                ignore += easy_params.ko_char
            if lang == "English":
                ignore += 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
            if lang == "Numbers":
                ignore += "0123456789"

        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        h, w = image.shape
        image = cv2.resize(image, (int(w * easy_params.imgH / h), easy_params.imgH))

        # create NN input
        image = cv2.dnn.blobFromImage(image, 1.0 / 255.)

        # Compute ONNX Runtime output prediction
        ort_inputs = {easy_params.ort_session.get_inputs()[0].name: image}
        ort_outs = easy_params.ort_session.run(None, ort_inputs)

        # Ignored indices
        ignore_char = easy_params.ignore_char + ignore
        ignore_char = [x for x in ignore_char if x in list(easy_params.char2id.keys())]
        ignore_idx = [easy_params.char2id[x] for x in ignore_char]

        # Ignore index + re-norm
        preds_prob = softmax(ort_outs[0], axis=2)
        preds_prob[:, :, ignore_idx] = 0.
        pred_norm = preds_prob.sum(axis=2)
        preds_prob = preds_prob / np.expand_dims(pred_norm, axis=-1)

        # Greedy Search over graph to get final decoded result
        decoded = greedy_decode(preds_prob, easy_params.id2char)

    return decoded


def ComtrueOCR(image, lang, ignore):
    if "QR/DM/Bar" in lang:
        decoded = CodeDecoder(image)
    else:
        all_lang = ["Korean", "English", "Numbers"]
        ignore_lang = list(set(all_lang).difference(lang))
        for lang in ignore_lang:
            if lang == "Korean":
                ignore += easy_params.ko_char
            if lang == "English":
                ignore += 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
            if lang == "Numbers":
                ignore += "0123456789"

        h, w, _ = image.shape
        # create NN input
        image = cv2.dnn.blobFromImage(image, 1.0 / 255.,
                                      (roundup(int(w * comtrue_params.imgH / h)), comtrue_params.imgH), swapRB=True)

        # Compute ONNX Runtime output prediction
        ort_inputs = {comtrue_params.ort_session.get_inputs()[0].name: image}
        ort_outs = comtrue_params.ort_session.run(None, ort_inputs)

        # Ignored indices
        ignore_char = comtrue_params.ignore_char + ignore
        ignore_char = [x for x in ignore_char if x in list(comtrue_params.char2id.keys())]
        ignore_idx = [comtrue_params.char2id[x] for x in ignore_char]

        # Ignore index + re-norm
        preds_prob = softmax(ort_outs[0], axis=2)
        preds_prob[:, :, ignore_idx] = 0.
        pred_norm = preds_prob.sum(axis=2)
        preds_prob = preds_prob / np.expand_dims(pred_norm, axis=-1)

        # Greedy Search over graph to get final decoded result
        decoded = greedy_decode(preds_prob, comtrue_params.id2char)

    return decoded
