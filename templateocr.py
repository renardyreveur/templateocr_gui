import math
import os
import sys

import json

import cv2
from PySide2 import QtWidgets, QtGui, QtCore
from PySide2.QtWidgets import QMainWindow, QFileDialog
import qimage2ndarray as q2nd

from ui import Ui_AiseeWindow, ui_config
from utils import TabPage, TestPage, ResizableRubberBand, create_template, load_template

TEMPLATES_DIR = os.path.join(QtCore.QDir.currentPath(), "templates")


# Main Window - Inherits the UI class (from QT Designer)
class TemplateOCR(QMainWindow, Ui_AiseeWindow):
    def __init__(self):
        super().__init__()
        # Use the ui schema defined by the python class in Ui_AiseeWindow
        self.setupUi(self)

        # language configs
        self.cfg = ui_config.cfg(lang="kor")

        # LOGOs
        self.label_7.setPixmap(QtGui.QPixmap("resources/ocr.png"))
        self.label_6.setPixmap(QtGui.QPixmap("resources/cometrue_ai_color.png"))

        # For image zoom
        self.scaleFactor = 0.0

        # OCR Fields
        self.ocr_fields = {}

        # Palettes for Special Buttons
        self.add_region_btn.setPalette(QtGui.QColor(6, 152, 154))
        self.reset_regions_btn.setPalette(QtGui.QColor(239, 41, 41))
        self.test_btn.setPalette(QtGui.QColor(115, 210, 22))
        self.save_btn.setPalette(QtGui.QColor(91, 127, 166))
        self.select_file_btn.setPalette(QtGui.QColor(91, 127, 166))

        # Button event handlers
        self.add_region_btn.clicked.connect(self.addRegion)
        self.select_file_btn.clicked.connect(self.filedialog)
        self.plus_btn.clicked.connect(self.zoomIn)
        self.minus_btn.clicked.connect(self.zoomOut)
        self.reset_zoom_btn.clicked.connect(self.normalSize)
        self.reset_regions_btn.clicked.connect(self.reset_regions)

        self.test_btn.clicked.connect(self.run_test)
        self.open_template_btn.clicked.connect(self.open_temp)
        self.save_btn.clicked.connect(self.save)

        self.tabWidget.currentChanged.connect(self.rubberband_update)
        self.head_set = False

    def rubberband_update(self):
        for _, rband in self.ocr_fields.items():
            rband.repaint()

    def run_test(self):
        dialog = TestPage(self)
        templates = os.listdir(TEMPLATES_DIR)
        templates.remove("placeholder")
        templates = [x[:-5] for x in templates]
        for temp in templates:
            list_item = QtWidgets.QListWidgetItem(temp)
            list_item.setCheckState(QtCore.Qt.Unchecked)
            dialog.template_list.addItem(list_item)

        dialog.setWindowTitle(self.cfg.testing_str)
        dialog.exec_()

    def save(self):
        field_empty = False
        head_exists = False
        lang_exists = True
        for i in range(self.tabWidget.count()):
            if self.tabWidget.widget(i).head_field_check.checkState() == QtCore.Qt.Checked:
                head_exists = True
            if self.tabWidget.widget(i).field_name.text().strip(" ") == "":
                field_empty = True
            checked = 0
            check_list = self.tabWidget.widget(i).groupBox_2.findChildren(QtWidgets.QCheckBox)
            for box in check_list:
                if box.isChecked():
                    checked += 1
            if checked == 0:
                lang_exists = False

        if self.tabWidget.count() == 0:
            msgBox = QtWidgets.QMessageBox()
            msgBox.setWindowTitle(self.cfg.error_str)
            msgBox.setText(self.cfg.one_field_str)
            msgBox.exec_()

        elif self.template_name.text().strip(" ") == "":
            msgBox = QtWidgets.QMessageBox()
            msgBox.setWindowTitle(self.cfg.error_str)
            msgBox.setText(self.cfg.temp_name_str)
            msgBox.exec_()

        elif field_empty:
            msgBox = QtWidgets.QMessageBox()
            msgBox.setWindowTitle(self.cfg.error_str)
            msgBox.setText(self.cfg.field_empty_str)
            msgBox.exec_()

        elif head_exists is False:
            msgBox = QtWidgets.QMessageBox()
            msgBox.setWindowTitle(self.cfg.error_str)
            msgBox.setText(self.cfg.head_field_str)
            msgBox.exec_()

        elif self.headtextEdit.toPlainText().strip(" ") == "":
            msgBox = QtWidgets.QMessageBox()
            msgBox.setWindowTitle(self.cfg.error_str)
            msgBox.setText(self.cfg.head_text_str)
            msgBox.exec_()

        elif not lang_exists:
            msgBox = QtWidgets.QMessageBox()
            msgBox.setWindowTitle(self.cfg.error_str)
            msgBox.setText(self.cfg.language_str)
            msgBox.exec_()

        else:
            template_dict = create_template(self)
            with open(os.path.join("templates", self.template_name.text() + ".json"), 'w') as f:
                json.dump(template_dict, f)

            msgBox = QtWidgets.QMessageBox()
            msgBox.setWindowTitle(self.cfg.success_str)
            msgBox.setText(self.cfg.saved_temp_str)
            msgBox.exec_()

    def open_temp(self):
        if self.label.pixmap() is None:
            msgBox = QtWidgets.QMessageBox()
            msgBox.setWindowTitle(self.cfg.error_str)
            msgBox.setText(self.cfg.open_image_str)
            msgBox.exec_()
        else:
            dialog = QFileDialog(self)
            dialog.setOption(QtWidgets.QFileDialog.DontUseNativeDialog)
            dialog.setWindowTitle(self.cfg.open_temp_str)
            dialog.setDirectory(TEMPLATES_DIR)
            # TODO: Restrict Access to template folder only
            # dialog.directoryEntered.connect(dialog.setDirectory(TEMPLATES_DIR))
            dialog.setNameFilter('Template Files (*.json)')
            dialog.setFileMode(QFileDialog.ExistingFile)

            filename = None
            if dialog.exec_() == QtWidgets.QDialog.Accepted:
                filename = dialog.selectedFiles()[0]

            if filename is not None:
                with open(filename, 'r') as f:
                    template_info = json.load(f)

                    for _, rband in self.ocr_fields.items():
                        rband.deleteLater()
                    for i in range(self.tabWidget.count()):
                        self.tabWidget.removeTab(0)

                    self.ocr_fields = {}
                    self.normalSize()
                    load_template(self, template_info)

    # Delete all ocr fields  + Delete all rubber bands
    def reset_regions(self):
        if len(self.ocr_fields) > 0:
            msgBox = QtWidgets.QMessageBox()
            msgBox.setWindowTitle(self.cfg.error_str)
            msgBox.setText(self.cfg.reg_del_str)
            msgBox.setInformativeText(self.cfg.continue_str)
            msgBox.setStandardButtons(QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
            msgBox.setDefaultButton(QtWidgets.QMessageBox.No)
            reply = msgBox.exec_()

            if reply == QtWidgets.QMessageBox.Yes:
                for _, rband in self.ocr_fields.items():
                    rband.close()
                    rband.deleteLater()
                for i in range(self.tabWidget.count()):
                    self.tabWidget.removeTab(0)

                self.head_set = False

                self.ocr_fields = {}

    # Slot for the Open Image Button clicked signal
    def filedialog(self):
        """
         Open a File dialog, read the image with OpenCV then display it as a QPixmap
        """
        dialog = QFileDialog(self)
        dialog.setOption(QtWidgets.QFileDialog.DontUseNativeDialog)
        dialog.setWindowTitle(self.cfg.op_img_dia_str)
        dialog.setNameFilter('Image Files (*.png *.jpg *.bmp)')

        dialog.setFileMode(QFileDialog.ExistingFile)
        path = None
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            path = dialog.selectedFiles()
            for _, rband in self.ocr_fields.items():
                rband.deleteLater()
            for i in range(self.tabWidget.count()):
                self.tabWidget.removeTab(0)

            self.ocr_fields = {}

            # Reset fields
            self.template_name.setText("")
            self.headtextEdit.setText("")
            self.head_set = False

        if path:
            image = cv2.imread(path[0])
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            image = q2nd.array2qimage(image)
            image = QtGui.QPixmap.fromImage(image)

            self.label.setPixmap(image)
            self.label.adjustSize()  # Get image size and make it the label size
            # Change the minimumSize of the scroll container to the label size (to make scrollbars appear)
            self.scrollAreaWidgetContents.setMinimumSize(self.label.size())

            self.scaleFactor = 1.0

            self.preview.setVisible(True)

    # Slot for the add Region button clicked signal
    def addRegion(self):
        # Add a Resizable Rubber band with default size
        if self.label.pixmap() is not None:
            field_id = max(list(self.ocr_fields.keys())) + 1 if len(list(self.ocr_fields.keys())) > 0 else 1
            rband = ResizableRubberBand(self.label, field_id)
            rband.setGeometry(self.preview.horizontalScrollBar().value() + 70, self.preview.verticalScrollBar().value() + 70,
                              self.preview.width()//5, self.preview.height()//5)
            self.ocr_fields.update({field_id: rband})
            self.addNewTab(field_id)

    # Add New tab with new field
    def addNewTab(self, f_id):
        self.tabWidget.addTab(TabPage(self.tabWidget), str(f_id))

    # Zoom in/out + Reset buttons
    # TODO: Make the rubber bands stay in the relative positions when zoomed in and out
    def zoomIn(self):
        if self.label.pixmap() is not None:
            self.scaleImage(1.25)
            if self.scaleFactor > 2.5:
                self.plus_btn.setDisabled(True)
            else:
                self.plus_btn.setEnabled(True)
                self.minus_btn.setEnabled(True)

    def zoomOut(self):
        if self.label.pixmap() is not None:
            self.scaleImage(0.8)
            if self.scaleFactor < 0.5:
                self.minus_btn.setDisabled(True)
            else:
                self.plus_btn.setEnabled(True)
                self.minus_btn.setEnabled(True)

    def normalSize(self):
        if self.label.pixmap() is not None:
            self.label.adjustSize()

            factor = 1.0/self.scaleFactor
            for _, rband in self.ocr_fields.items():
                rband.factor *= factor
                if factor > 1:
                    rband.resize(QtCore.QSize(math.floor(factor * rband.width()), math.floor(factor * rband.height())))
                    rband.move(math.ceil(rband.x() * factor), math.ceil(rband.y() * factor))
                else:
                    rband.resize(QtCore.QSize(math.ceil(factor * rband.width()), math.ceil(factor * rband.height())))
                    rband.move(math.floor(rband.x() * factor), math.floor(rband.y() * factor))

            self.scaleFactor = 1.0
            self.zoom_level.setText("{:d}%".format(int(self.scaleFactor*100)))
            self.plus_btn.setEnabled(True)
            self.minus_btn.setEnabled(True)
            self.scrollAreaWidgetContents.setMinimumSize(self.label.size())

    def scaleImage(self, factor):
        self.scaleFactor *= factor
        self.zoom_level.setText("{:d}%".format(int(self.scaleFactor*100)))

        # Resize image to the zoom level, change the scroll container minimumSize accordingly
        self.label.resize(factor * self.label.size())

        # Resize the rubber bands accordingly
        for _, rband in self.ocr_fields.items():
            rband.factor *= factor
            if factor > 1:
                rband.resize(QtCore.QSize(math.floor(factor * rband.width()), math.floor(factor*rband.height())))
                rband.move(math.ceil(rband.x()*factor), math.ceil(rband.y()*factor))
            else:
                rband.resize(QtCore.QSize(math.ceil(factor * rband.width()), math.ceil(factor*rband.height())))
                rband.move(math.floor(rband.x()*factor), math.floor(rband.y()*factor))

        self.scrollAreaWidgetContents.setMinimumSize(self.label.size())

        self.adjustScrollBar(self.preview.horizontalScrollBar(), factor)
        self.adjustScrollBar(self.preview.verticalScrollBar(), factor)

    @staticmethod
    def adjustScrollBar(scrollbar, factor):
        scrollbar.setValue(int(factor * scrollbar.value()
                               + ((factor - 1) * scrollbar.pageStep() / 2)))


if __name__ == "__main__":
    # Application Handler - holds event loop of application
    app = QtWidgets.QApplication([])

    # Create Qt Widget => Our window
    widget = TemplateOCR()
    widget.show()  # Windows are hidden by default

    sys.exit(app.exec_())
