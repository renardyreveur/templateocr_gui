
class cfg:
    def __init__(self, lang):
        if lang == "kor":
            self.testing_str = "테스트 중!"
            self.error_str = "주의!"
            self.success_str = "성공!"
            self.done_str = "완료!"
            self.one_field_str = "최소 한개 이상의 영역을 지정해주세요!"
            self.temp_name_str = "템플렛 이름을 지정해주세요!"
            self.field_empty_str = "필드 영역의 명칭이 비어있는 영역이 있습니다!"
            self.head_field_str = "대표 영역을 선택해 주세요!"
            self.head_text_str = "대표 영역 문자열을 적어주세요!"
            self.language_str = "영역 인식 종류를 최소한 한개 인상 선택해 주세요!"
            self.saved_temp_str = "템플렛이 성공적으로 저장되었습니다!"
            self.open_image_str = "이미지를 먼저 열어주세요!"
            self.open_temp_str = "템플렛 열기"
            self.reg_del_str = "모든 영역을 삭제합니다!"
            self.continue_str = "진행하시겠습니까?"
            self.op_img_dia_str = "이미지 열기"

            # test_temp
            self.saved_res_str = "결과가 {}에 저장되었습니다!"
            self.running_test_str = "템플렛 OCR 테스트 진행 중..."
            self.total_img_str = "총 {}개의 이미지를 검사합니다..."
            self.abort_str = "테스트 취소"

            # test_page
            self.test_folder_str = "테스트 폴더 경로를 지정해 주세요!"
            self.one_temp_str = "최소 한개의 템플렛을 설정해 주세요!"
            self.test_fol_dia_str = "테스트 폴더 지정"


        else:
            self.testing_str = "Testing!"
            self.error_str = "Error!"
            self.success_str = "Success!"
            self.done_str = "Done!"
            self.one_field_str = "At least one field is required!"
            self.temp_name_str = "Template Name required!"
            self.field_empty_str = "At least one of the field names is empty!"
            self.head_field_str = "Select a Head Field!"
            self.head_text_str = "Head Field Text required!"
            self.language_str = "One of the regions don't have any recognition type set!"
            self.saved_temp_str = "Saved Template!"
            self.open_image_str = "Open an Image First!"
            self.open_temp_str = "Open Template"
            self.reg_del_str = "This will remove all fields on screen"
            self.continue_str = "Would you like to proceed?"
            self.op_img_dia_str = "Open Image"

            # test_temp
            self.saved_res_str = "Results saved in {}"
            self.running_test_str = "Running Template OCR..."
            self.total_img_str = "Extracting text from {} images..."
            self.abort_str = "Abort"

            # test_page
            self.test_folder_str = "Please choose a path for the Testing Directory!"
            self.one_temp_str = "Select at least 1 template!"
            self.test_fol_dia_str = "Choose Testing Folder"
