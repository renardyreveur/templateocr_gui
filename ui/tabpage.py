# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'tabpage_kor.ui'
##
## Created by: Qt User Interface Compiler version 5.15.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import (QCoreApplication, QDate, QDateTime, QMetaObject,
    QObject, QPoint, QRect, QSize, QTime, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
    QFontDatabase, QIcon, QKeySequence, QLinearGradient, QPalette, QPainter,
    QPixmap, QRadialGradient)
from PySide2.QtWidgets import *


class Ui_TabPage(object):
    def setupUi(self, TabPage):
        if not TabPage.objectName():
            TabPage.setObjectName(u"TabPage")
        TabPage.resize(364, 391)
        self.verticalLayout_3 = QVBoxLayout(TabPage)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.groupBox = QGroupBox(TabPage)
        self.groupBox.setObjectName(u"groupBox")
        self.verticalLayout = QVBoxLayout(self.groupBox)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.label = QLabel(self.groupBox)
        self.label.setObjectName(u"label")
        self.label.setMaximumSize(QSize(16777215, 20))

        self.verticalLayout.addWidget(self.label)

        self.field_name = QLineEdit(self.groupBox)
        self.field_name.setObjectName(u"field_name")

        self.verticalLayout.addWidget(self.field_name)

        self.line = QFrame(self.groupBox)
        self.line.setObjectName(u"line")
        self.line.setFrameShape(QFrame.HLine)
        self.line.setFrameShadow(QFrame.Sunken)

        self.verticalLayout.addWidget(self.line)

        self.head_field_check = QCheckBox(self.groupBox)
        self.head_field_check.setObjectName(u"head_field_check")

        self.verticalLayout.addWidget(self.head_field_check)

        self.line_2 = QFrame(self.groupBox)
        self.line_2.setObjectName(u"line_2")
        self.line_2.setFrameShape(QFrame.HLine)
        self.line_2.setFrameShadow(QFrame.Sunken)

        self.verticalLayout.addWidget(self.line_2)

        self.groupBox_2 = QGroupBox(self.groupBox)
        self.groupBox_2.setObjectName(u"groupBox_2")
        self.groupBox_2.setMaximumSize(QSize(16777215, 65))
        self.horizontalLayout_2 = QHBoxLayout(self.groupBox_2)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.num_type = QCheckBox(self.groupBox_2)
        self.num_type.setObjectName(u"num_type")
        self.num_type.setChecked(True)

        self.horizontalLayout.addWidget(self.num_type)

        self.kor_type = QCheckBox(self.groupBox_2)
        self.kor_type.setObjectName(u"kor_type")
        self.kor_type.setChecked(True)

        self.horizontalLayout.addWidget(self.kor_type)

        self.eng_type = QCheckBox(self.groupBox_2)
        self.eng_type.setObjectName(u"eng_type")
        self.eng_type.setChecked(True)

        self.horizontalLayout.addWidget(self.eng_type)

        self.code_type = QCheckBox(self.groupBox_2)
        self.code_type.setObjectName(u"code_type")
        self.code_type.setEnabled(True)

        self.horizontalLayout.addWidget(self.code_type)


        self.horizontalLayout_2.addLayout(self.horizontalLayout)


        self.verticalLayout.addWidget(self.groupBox_2)

        self.line_3 = QFrame(self.groupBox)
        self.line_3.setObjectName(u"line_3")
        self.line_3.setFrameShape(QFrame.HLine)
        self.line_3.setFrameShadow(QFrame.Sunken)

        self.verticalLayout.addWidget(self.line_3)

        self.label_2 = QLabel(self.groupBox)
        self.label_2.setObjectName(u"label_2")

        self.verticalLayout.addWidget(self.label_2)

        self.char_black = QTextEdit(self.groupBox)
        self.char_black.setObjectName(u"char_black")
        self.char_black.setMaximumSize(QSize(16777215, 60))

        self.verticalLayout.addWidget(self.char_black)

        self.pushButton = QPushButton(self.groupBox)
        self.pushButton.setObjectName(u"pushButton")
        palette = QPalette()
        brush = QBrush(QColor(0, 0, 0, 255))
        brush.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.WindowText, brush)
        brush1 = QBrush(QColor(239, 41, 41, 255))
        brush1.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.Button, brush1)
        brush2 = QBrush(QColor(255, 147, 147, 255))
        brush2.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.Light, brush2)
        brush3 = QBrush(QColor(247, 94, 94, 255))
        brush3.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.Midlight, brush3)
        brush4 = QBrush(QColor(119, 20, 20, 255))
        brush4.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.Dark, brush4)
        brush5 = QBrush(QColor(159, 27, 27, 255))
        brush5.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.Mid, brush5)
        palette.setBrush(QPalette.Active, QPalette.Text, brush)
        brush6 = QBrush(QColor(255, 255, 255, 255))
        brush6.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.BrightText, brush6)
        palette.setBrush(QPalette.Active, QPalette.ButtonText, brush)
        palette.setBrush(QPalette.Active, QPalette.Base, brush6)
        palette.setBrush(QPalette.Active, QPalette.Window, brush1)
        palette.setBrush(QPalette.Active, QPalette.Shadow, brush)
        brush7 = QBrush(QColor(247, 148, 148, 255))
        brush7.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.AlternateBase, brush7)
        brush8 = QBrush(QColor(255, 255, 220, 255))
        brush8.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.ToolTipBase, brush8)
        palette.setBrush(QPalette.Active, QPalette.ToolTipText, brush)
        palette.setBrush(QPalette.Inactive, QPalette.WindowText, brush)
        palette.setBrush(QPalette.Inactive, QPalette.Button, brush1)
        palette.setBrush(QPalette.Inactive, QPalette.Light, brush2)
        palette.setBrush(QPalette.Inactive, QPalette.Midlight, brush3)
        palette.setBrush(QPalette.Inactive, QPalette.Dark, brush4)
        palette.setBrush(QPalette.Inactive, QPalette.Mid, brush5)
        palette.setBrush(QPalette.Inactive, QPalette.Text, brush)
        palette.setBrush(QPalette.Inactive, QPalette.BrightText, brush6)
        palette.setBrush(QPalette.Inactive, QPalette.ButtonText, brush)
        palette.setBrush(QPalette.Inactive, QPalette.Base, brush6)
        palette.setBrush(QPalette.Inactive, QPalette.Window, brush1)
        palette.setBrush(QPalette.Inactive, QPalette.Shadow, brush)
        palette.setBrush(QPalette.Inactive, QPalette.AlternateBase, brush7)
        palette.setBrush(QPalette.Inactive, QPalette.ToolTipBase, brush8)
        palette.setBrush(QPalette.Inactive, QPalette.ToolTipText, brush)
        palette.setBrush(QPalette.Disabled, QPalette.WindowText, brush4)
        palette.setBrush(QPalette.Disabled, QPalette.Button, brush1)
        palette.setBrush(QPalette.Disabled, QPalette.Light, brush2)
        palette.setBrush(QPalette.Disabled, QPalette.Midlight, brush3)
        palette.setBrush(QPalette.Disabled, QPalette.Dark, brush4)
        palette.setBrush(QPalette.Disabled, QPalette.Mid, brush5)
        palette.setBrush(QPalette.Disabled, QPalette.Text, brush4)
        palette.setBrush(QPalette.Disabled, QPalette.BrightText, brush6)
        palette.setBrush(QPalette.Disabled, QPalette.ButtonText, brush4)
        palette.setBrush(QPalette.Disabled, QPalette.Base, brush1)
        palette.setBrush(QPalette.Disabled, QPalette.Window, brush1)
        palette.setBrush(QPalette.Disabled, QPalette.Shadow, brush)
        palette.setBrush(QPalette.Disabled, QPalette.AlternateBase, brush1)
        palette.setBrush(QPalette.Disabled, QPalette.ToolTipBase, brush8)
        palette.setBrush(QPalette.Disabled, QPalette.ToolTipText, brush)
        self.pushButton.setPalette(palette)

        self.verticalLayout.addWidget(self.pushButton)


        self.verticalLayout_3.addWidget(self.groupBox)


        self.retranslateUi(TabPage)

        QMetaObject.connectSlotsByName(TabPage)
    # setupUi

    def retranslateUi(self, TabPage):
        TabPage.setWindowTitle(QCoreApplication.translate("TabPage", u"Form", None))
        self.groupBox.setTitle(QCoreApplication.translate("TabPage", u"OCR \uc601\uc5ed", None))
        self.label.setText(QCoreApplication.translate("TabPage", u"\uc601\uc5ed \uc774\ub984", None))
        self.head_field_check.setText(QCoreApplication.translate("TabPage", u"\ub300\ud45c \uc601\uc5ed", None))
        self.groupBox_2.setTitle(QCoreApplication.translate("TabPage", u"\uc601\uc5ed \uc778\uc2dd \uc885\ub958", None))
        self.num_type.setText(QCoreApplication.translate("TabPage", u"\uc601\uc5b4", None))
        self.kor_type.setText(QCoreApplication.translate("TabPage", u"\ud55c\uad6d\uc5b4", None))
        self.eng_type.setText(QCoreApplication.translate("TabPage", u"\uc22b\uc790", None))
        self.code_type.setText(QCoreApplication.translate("TabPage", u"QR/DM/Bar", None))
        self.label_2.setText(QCoreApplication.translate("TabPage", u"\ucca0\uc790 \ube14\ub799\ub9ac\uc2a4\ud2b8", None))
        self.pushButton.setText(QCoreApplication.translate("TabPage", u"\uc601\uc5ed \uc0ad\uc81c", None))
    # retranslateUi

