# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'test_dialog_kor.ui'
##
## Created by: Qt User Interface Compiler version 5.15.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import (QCoreApplication, QDate, QDateTime, QMetaObject,
    QObject, QPoint, QRect, QSize, QTime, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
    QFontDatabase, QIcon, QKeySequence, QLinearGradient, QPalette, QPainter,
    QPixmap, QRadialGradient)
from PySide2.QtWidgets import *


class Ui_test_dialog(object):
    def setupUi(self, test_dialog):
        if not test_dialog.objectName():
            test_dialog.setObjectName(u"test_dialog")
        test_dialog.resize(517, 586)
        self.verticalLayout = QVBoxLayout(test_dialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.label_2 = QLabel(test_dialog)
        self.label_2.setObjectName(u"label_2")
        font = QFont()
        font.setPointSize(13)
        self.label_2.setFont(font)

        self.verticalLayout.addWidget(self.label_2)

        self.template_list = QListWidget(test_dialog)
        self.template_list.setObjectName(u"template_list")
        self.template_list.setSpacing(6)

        self.verticalLayout.addWidget(self.template_list)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.pushButton_2 = QPushButton(test_dialog)
        self.pushButton_2.setObjectName(u"pushButton_2")

        self.horizontalLayout.addWidget(self.pushButton_2)

        self.pushButton = QPushButton(test_dialog)
        self.pushButton.setObjectName(u"pushButton")

        self.horizontalLayout.addWidget(self.pushButton)


        self.verticalLayout.addLayout(self.horizontalLayout)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer)

        self.label = QLabel(test_dialog)
        self.label.setObjectName(u"label")
        self.label.setFont(font)

        self.verticalLayout.addWidget(self.label)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.lineEdit = QLineEdit(test_dialog)
        self.lineEdit.setObjectName(u"lineEdit")
        self.lineEdit.setEnabled(False)

        self.horizontalLayout_2.addWidget(self.lineEdit)

        self.pushButton_3 = QPushButton(test_dialog)
        self.pushButton_3.setObjectName(u"pushButton_3")

        self.horizontalLayout_2.addWidget(self.pushButton_3)


        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer_2)

        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_3.addItem(self.horizontalSpacer_2)

        self.cancel = QPushButton(test_dialog)
        self.cancel.setObjectName(u"cancel")

        self.horizontalLayout_3.addWidget(self.cancel)

        self.ok = QPushButton(test_dialog)
        self.ok.setObjectName(u"ok")

        self.horizontalLayout_3.addWidget(self.ok)


        self.verticalLayout.addLayout(self.horizontalLayout_3)

        self.verticalLayout.setStretch(1, 1)

        self.retranslateUi(test_dialog)

        QMetaObject.connectSlotsByName(test_dialog)
    # setupUi

    def retranslateUi(self, test_dialog):
        test_dialog.setWindowTitle(QCoreApplication.translate("test_dialog", u"Dialog", None))
        self.label_2.setText(QCoreApplication.translate("test_dialog", u"\ud14c\uc2a4\ud2b8 \ud15c\ud50c\ub81b \uc124\uc815", None))
        self.pushButton_2.setText(QCoreApplication.translate("test_dialog", u"\uc120\ud0dd \ucd08\uae30\ud654", None))
        self.pushButton.setText(QCoreApplication.translate("test_dialog", u"\ubaa8\ub450 \uc120\ud0dd", None))
        self.label.setText(QCoreApplication.translate("test_dialog", u"\ud14c\uc2a4\ud2b8 \ud3f4\ub354 \uc9c0\uc815", None))
        self.pushButton_3.setText(QCoreApplication.translate("test_dialog", u"\ud3f4\ub354 \uc5f4\uae30", None))
        self.cancel.setText(QCoreApplication.translate("test_dialog", u"\u274c \ucde8\uc18c", None))
        self.ok.setText(QCoreApplication.translate("test_dialog", u"\u2714 \ud14c\uc2a4\ud2b8", None))
    # retranslateUi

